#include <iostream>

class Base {
public:
    void func() {                                                           // non-virtual thus no polymorphism
        std::cout << "Base: " << field_ << std::endl;
    }

protected:
    std::string field_;
};

class Child : public Base {
public:
    Child(const std::string& field) {
        field_ = field;
    }

    void func() {
        std::cout << "Child: " << field_ << std::endl;
    }
};

int main() {
    Child child("foo");
    Base base = child;                                                      // implicit conversion to base class

    child.func();
    base.func();

    Base* base_ptr = &child;                                                // implicit conversion of pointer
    Base& bref = child;                                                     // implicit conversion of ref

    base_ptr->func();
    bref.func();
    
    return 0;
}
