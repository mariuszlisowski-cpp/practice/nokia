#include <iostream>

class A {
public:
    void func() {
        std::cout << "func() was called" << std::endl;
    }
};

class B : virtual public A {};                                      // virtual NOW

class C : virtual public A {};                                      // virtual NOW

class D : public B, public C {};                                    // multi-inheritance
                                                                    // func ingerited ONCE form A

/*    A
    /   \
   B     C                                                          // MUST be virtual
    \   /
      D                                                             // multi-ingerited from B & C
 */

int main() {
    D d;
    d.func();                                                       // not ambiguous any more

    return 0;
}
