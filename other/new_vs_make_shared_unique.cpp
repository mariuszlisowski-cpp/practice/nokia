#include <memory>

class MyData {};

void sink(std::shared_ptr<MyData> lhs, std::shared_ptr<MyData> rhs) {}

void use_new() {
    sink(std::shared_ptr<MyData>(new MyData()),                             // unspecified order
         std::shared_ptr<MyData>(new MyData()));                            // of objects creations
}

void use_make() {
    sink(std::make_shared<MyData>(),                                        // order guaranteed
         std::make_shared<MyData>());
}

int main() {
    /* auto p = new MyData(10); means
    1 allocate sizeof(MyData) bytes
    2 run MyData ctor
    3 assign address of allocated memory to p
    Order of evaluation of the operands
    of almost all C++ operators is unspecied */
    
    use_new();                                                              // unspecified order
    use_make();                                                             // proper order

    return 0;
}
