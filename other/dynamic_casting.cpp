#include <iostream>

struct  Granpa {
    virtual ~Granpa() {}
};

struct Parent1 : Granpa {};
struct Parent2 : Granpa {};

struct Child : Parent1 {};

int main() {
    Child obj;
    Granpa* p = &obj;

    Parent1* p1 = dynamic_cast<Parent1*>(p);                        // ok
    Parent2* p2 = dynamic_cast<Parent2*>(p);                        // fail
    Child* p3 = dynamic_cast<Child*>(p);                            // ok
    
    std::cout << std::boolalpha << (p1 != nullptr) << std::endl;
    std::cout << std::boolalpha << (p2 != nullptr) << std::endl;
    std::cout << std::boolalpha << (p3 != nullptr) << std::endl;

    return 0;
}
