#include <iostream>

class Base {
public:
    virtual ~Base() {}
    virtual void show() = 0;                                // abstract class as method is pure virtual
    void saySomething() {
        std::cout << "Hi, I'm base" << std::endl;
    }
};

class Derived : public Base {
public:
    void show() override {
        std::cout << "I'm derived" << std::endl;
    }
};

int main() {
    Base* obj = new Derived;
    // Base* abstract = new Base;                           // abstract class thus object not allowed

    obj->show();
    obj->saySomething();

    delete obj;

    return 0;
}
