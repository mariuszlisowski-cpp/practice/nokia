/* Binding means matching the function call with the correct function definition by the compiler.
   It takes place either at compile time or at runtime.
*/
#include <iostream>
    
/* late binding / dynamic binding / runtime binding / polymorphism */
class Base {
public:
    virtual void show() {                                                   // polymorphism as virtual
        std::cout<<"base class method\n";
    }
};
    
class Derived: public Base {
public:
    void show() {
        std::cout<<"derived class method\n";
    }
};
    
int main(void) {
    Base* base_ptr = new Derived;
  
    base_ptr->show();                                                       // derived method called
  
    return 0;
}
