#include <iostream>

struct A {
  A() { std::cout << "A()" << std::endl; }
  virtual ~A() { std::cout << "~A()" << std::endl; }                    // MUST  be virtual

  virtual void foo() {}
};

class B : public A {
public:
  B() { std::cout << "B()" << std::endl; }
  ~B() { std::cout << "~B()" << std::endl; }                            // so this d'tor could be called
};

int main() {
  A *ptr = new B;
  delete ptr;                                                           // B' destructor called now

  return 0;
}
