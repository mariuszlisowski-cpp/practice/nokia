#include <iostream>

class A {
public:
    virtual void func() {
        std::cout << "A func() was called" << std::endl;
    }
};

class B : virtual public A {                                        // virtual NOW
public:
    void func() override {
        std::cout << "B func() was called" << std::endl;
    }
};

class C : virtual public A {                                        // virtual NOW
public:
    void func() override {
        std::cout << "C func() was called" << std::endl;
    }
};

class D : public B, public C {                                      // multi-inheritance
public:
    void func() override {
        std::cout << "D func() was called" << std::endl;
    }
};

/*    A
    /   \
   B     C                                                          // MUST be virtual
    \   /
      D                                                             // multi-ingerited from B & C
 */

int main() {
    D d;
    d.func();                                                       // not ambiguous any more

    return 0;
}
