// compile: g++ -g sanitize_gcc.cpp -o sanitize_gcc -fsanitize=address

int main() {
    /* not detected! */
    int* ptr = new int(9);
    // delete ptr;                                      // memory leak!

    /* detected */
    int* arr = new int[10];
    int el = arr[10];                                   // out of bound!

    return 0;
}
