#include <iostream>
#include <memory>
#include <vector>

struct Foo {
    void tell() {
        std::cout << "I'm foo" << std::endl;
    }
};

int main() {
    std::unique_ptr<Foo[]> foos(new Foo[10]);
    foos[0].tell();
    foos[9].tell();

    std::vector<std::unique_ptr<Foo>> foos_v2;
    foos_v2.push_back(std::make_unique<Foo>());                             // pushing later
    foos_v2.push_back(std::make_unique<Foo>());

    return 0;
}
