#include <cstddef>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

struct Bar {
    Bar(int number) : number_(number) {}                                    // parametrized c'tor

    int number_;
};

struct Foo {
    Foo(int number, const std::string& name)                                // parametrized c'tor
        : number_(number)
        , name_(name) {}

    void tell() {
        std::cout << "I'm foo with no " << number_ << std::endl;
    }

    int number_;
    std::string name_;
};

template<typename T>
void print(T& container) {
    for (const auto& el : container) {
        std::cout << el.number_ << ' ';
    }
}

int main() {
    /* there is no way to create an array of objects 
       with parameterized constructor with a raw array
    Foo* ptr = new Foo[10];
     */

    /* use vector instead */
    const size_t size = 10;
    std::vector<Foo> foos(size, Foo(77, "foo"));                            // initialization...
    print(foos);                                                            // with double arguments
    
    std::vector<Bar> bars(size, 99);                                        // with single argument
    print(bars);

    return 0;
}
