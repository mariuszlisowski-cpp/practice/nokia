/* static polymorphism is also known as
   early binding and 
   compile-time polymorphism.
*/
#include <iostream>
#include <string>

// method
void funct(int x) {
    std::cout << x << std::endl;
}

// overloaded method
void funct(const std::string& x) {
    std::cout << x << std::endl;
}

// overloaded operator
class Clazz {
public:
    Clazz& operator+(const Clazz& other);
};

int main() {
    funct(100);
    funct("1");

    return 0;
}
