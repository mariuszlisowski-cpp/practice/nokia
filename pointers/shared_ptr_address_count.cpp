#include <iostream>
#include <memory>

int main() {
    /* smart pointers */
    auto sptrA = std::make_shared<int>(42);

    auto sptrB = sptrA;                             // make a pointer copy (but share the location)
    
    std::cout << sptrA << std::endl;                // points to the same location
    std::cout << sptrB << std::endl;                // saa

    std::cout << sptrA.use_count() << std::endl;    // same count
    std::cout << sptrB.use_count() << std::endl;    // saa

    /* raw pointers */
    int* ptrA = new int(24);
    int* ptrB = ptrA;
    std::cout << ptrA << std::endl;                 // points to the same location
    std::cout << ptrB << std::endl;                 // saa
    
    delete ptrA;
    ptrA = nullptr;                                 // not valid any more
    ptrB = nullptr;                                 // saa

    return 0;
}
