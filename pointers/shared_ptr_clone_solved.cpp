#include <cassert>
#include <iostream>
#include <memory>

class Foo {
public:
    Foo(int value) : value_(value) {}

    std::shared_ptr<Foo> clone() {
        // return std::shared_ptr<Foo>(this);                           // ERROR: double ownership (use_count == 1)
        // return std::shared_ptr<Foo>(new Foo(*this));                 // OK: actually clones
        return std::make_shared<Foo>(*this);                            // OK: saa
    }

    bool operator==(const Foo& other) const {
        return true;
    }

    int value_;
};

int main() {
    /* create new object using default constructor */
    std::shared_ptr<Foo> origin = std::make_shared<Foo>(42);

    /* create new object using copy constructor constructor */
    std::shared_ptr<Foo> cloned = std::make_shared<Foo>(*origin);       // independent object (with value 42)
 
    /* create new shared ptr */
    std::shared_ptr<Foo> origin_shared = std::shared_ptr<Foo>(origin);  // pointing to the same object

    /* testing */    
    std::cout << "origin count: " << origin.use_count() << std::endl;   // one object
    std::cout << "shared count: " << origin_shared.use_count()          // SAME object
              << std::endl;

    std::cout << "clone count: " << cloned.use_count() << std::endl;    // DIFFERENT object

    origin->value_ = 24;                                                // origin changed...
    std::cout << origin_shared->value_ << std::endl;                    // thus its shared also changed...

    std::cout << cloned->value_ << std::endl;                           // but clone is DIFFERENT (still with 42)

    return 0;
}
