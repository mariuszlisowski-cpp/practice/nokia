#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>

class Foo {
public:
    Foo(int value) : value_(value) {}

    std::shared_ptr<Foo> clone() {
        return std::shared_ptr<Foo>(this);                              // ERROR: double ownership (use_count == 1)
        // return std::shared_ptr<Foo>(new Foo(*this));                 // OK: actually clones
        // return std::make_shared<Foo>(*this);                         // OK: saa
    }

    bool operator==(const Foo& other) const {
        return true;
    }

    int value_;
};

int main() {
    /* create new object using default constructor */
    std::shared_ptr<Foo> origin = std::make_shared<Foo>(42);

    /* create new shared ptr BUT should create new object*/
    std::shared_ptr<Foo> cloned = origin->clone();                      // SAME object (with value 42)
 
    /* testing */    
    std::cout << "origin count: " << origin.use_count() << '\n';        // one object
    std::cout << "clone count: " << cloned.use_count() << '\n';         // SAME object (actually NOT cloned)

    origin->value_ = 24;                                                // origin changed...
    std::cout << cloned->value_ << std::endl;                           // thus its cloned also changed...

    std::getchar();                                                     // stop before CRASH

    return 0;                                                           // out of scope: TWO shared pointers
}                                                                       // calls destructor TWICE
