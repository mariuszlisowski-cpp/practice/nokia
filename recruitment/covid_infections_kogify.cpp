/* task by Sebastian Szczecina */
#include <initializer_list>
#include <iostream>
#include <vector>

class Infections {
public:
    Infections(std::initializer_list<int>&& list)
        : infections{list} {}

    void add_current_day_infections(unsigned number) {
        infections.push_back(number);
    }
    
    auto average_periodical_infections(unsigned period) {
        int sum{};
        auto size = infections.size() >= period
                                      ? period
                                      : infections.size();
        auto it_end = infections.rbegin() + size;
        for (auto it = infections.rbegin(); it != it_end; ++it) {
            std::cout << *it << ' ';    // verbose 
            sum += *it;
        }

        return sum / size;
    }
    
private:
    std::vector<int> infections;
};

int main() {
    Infections infections({20, 40, 60});
 
    infections.add_current_day_infections(80);
    infections.add_current_day_infections(100);

    auto result = infections.average_periodical_infections(3);
    std::cout << "\naverage: " << result << std::endl;

    return 0;
}
