/* check if braces are valid / balanced */

#include <iostream>
#include <unordered_set>
#include <stack>
#include <string>

bool are_brackets_balanced_switch(const std::string& str) {
    std::stack<char> stack;
    for (const char ch : str) {
        switch (ch) {                                                       // ignore all chars except braces
        case '(' : stack.push(')'); break;                                  // opening brackets checked first
        case '{' : stack.push('}'); break;
        case '[' : stack.push(']'); break;
        case ')' : case '}' : case ']':                                     // check closing brackets on the stack
            if (!stack.empty() &&                                           // if empty balanced so far
                ch == stack.top())                                          // thus do not pop
            {
                stack.pop();
            } else {
                return false;
            }
        }
    }

    return stack.empty();
}

bool are_brackets_balanced(const std::string& str) {
    std::unordered_set<char> brackets { '(', '[', '{', '}', ']', ')', };
    std::stack<char> stack;

    for (const char ch : str) {
        if (!brackets.count(ch)) {
            continue;                                                       // brackets passed only
        }
        if (ch == '(' || ch == '[' || ch == '{') {                          // opening brackets checks
            stack.push(ch);
        } else {                                                            // closing brackets passed only
            if (!stack.empty() && 
                (ch == ')' && stack.top() == '(' ||
                 ch == ']' && stack.top() == '[' ||
                 ch == '}' && stack.top() == '{'))
            {
                stack.pop();
            } else {
                return false;
            }
        }
    }

    return stack.empty();
}

int main() {
    std::string str = "({ hello }) my [ beautiful ]{ world}";

    std::cout << (are_brackets_balanced(str)
                  ? "balanced"
                  : "unbalanced") << std::endl;
    std::cout << (are_brackets_balanced_switch(str)
                  ? "balanced"
                  : "unbalanced") << std::endl;

    return 0;
}
