#include <iostream>
#include <thread>

unsigned int accountAmount;

std::mutex mx;
std::condition_variable cv;

void depositMoney() {
    // go to the bank etc...
    // wait in line...
    {
        std::unique_lock<std::mutex> lock(mx);
        std::cout << "Depositing money" << std::endl;
        accountAmount += 5000;
    }
    // Notify others we're finished
    cv.notify_all();
}

void withdrawMoney() {
    std::unique_lock<std::mutex> lock(mx);
    // Wait until we know the money is there
    cv.wait(lock);
    std::cout << "Withdrawing money" << std::endl;
    accountAmount -= 2000;
}
int main() {
    accountAmount = 0;
    
    // Run both threads simultaneously:
    std::thread deposit(depositMoney);
    std::thread withdraw(withdrawMoney);
    
    // Wait for both threads to finish
    deposit.join();
    withdraw.join();
    
    std::cout << "All transactions processed. Final amount: " << accountAmount << std::endl;
    
    return 0;
}
