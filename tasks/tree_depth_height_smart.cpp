#include <algorithm>
#include <iostream>
#include <memory>

template<typename T>
struct Node {
    Node(T value) : value(value) {}

    std::unique_ptr<Node> left{};
    std::unique_ptr<Node> right{};
    T value;
};

template<typename T>
size_t depthB(std::unique_ptr<Node<T>> root) {
    return root
           ? std::max(depthB(std::move(root->left)),
                      depthB(std::move(root->right))) + 1
           : 0;
}

template <typename T>
size_t depthA(std::unique_ptr<Node<T>> root) {                              // accepts a smart pointer
    if (not root) {
        return 0;
    }
    auto leftDepth = depthA(std::move(root->left));                         // use of a smart pointer
    auto rightDepth = depthA((std::move(root->right)));                     // saa

    return std::max(leftDepth, rightDepth) + 1;                             // start counting from 1
}

/* 1                1               0
                  /   \
   2             2     3            1
               /  \   / \
   3          4    5 6   7          2
            /  \
   4       8    9                   3
 */
auto create_tree() {
    auto root = std::make_unique<Node<int>>(1);

    root->left = std::make_unique<Node<int>>(2);
    root->left->left = std::make_unique<Node<int>>(4);
    root->left->left->left = std::make_unique<Node<int>>(8);
    root->left->left->right = std::make_unique<Node<int>>(9);
    root->left->right = std::make_unique<Node<int>>(5);
    root->right = std::make_unique<Node<int>>(3);
    root->right->left = std::make_unique<Node<int>>(6);
    root->right->right = std::make_unique<Node<int>>(7);

    return root;
}

int main() {
    auto root = create_tree();                                              // unique pointer
    std::cout << depthA(std::move(root)) << std::endl;                      // pass as a unique

    root = create_tree();
    std::cout << depthB(std::move(root)) << std::endl;                      // saa

    return 0;
}
