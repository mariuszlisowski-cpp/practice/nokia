#include <iostream>
#include <memory>

template<typename T>
struct Node {
    Node(T value) : value(value) {}

    Node* left{};
    Node* right{};
    T value;
};

template<typename T>
size_t depthA(Node<T>* root) {
    if (!root) {
        return 0;
    }
    auto left_depth = depthA(root->left);
    auto right_depth = depthA(root->right);

    return std::max(left_depth, right_depth) + 1;                           // one added ONCE
}

template<typename T>
size_t depthB(Node<T>* root) {
    return root
           ? std::max(depthB(root->left), 
                      depthB(root->right)) + 1
           : 0;
}   

/* 1                1               0
                  /   \
   2             2     3            1
               /  \   / \
   3          4    5 6   7          2
            /  \
   4       8    9                   3
 */

 auto create_tree() {
    Node<int>* root = new Node(1);

    root->left = new Node(2);
    root->left->left = new Node(4);
    root->left->left->left = new Node(8);
    root->left->left->right = new Node(9);
    root->left->right = new Node(5);
    root->right = new Node(3);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    return root;
}

int main() {
    auto root = create_tree();                                              // raw pointer

    std::cout << depthA(root) << std::endl;
    std::cout << depthB(root) << std::endl;                     

    return 0;
}
