#include <iostream>
#include <utility>

class Clazz {};

void f(const Clazz&) {                                              // accepts const lvalue reference
    std::cout << "const lvalue reference\n";
}
void f(Clazz&) {                                                    // accepts lvalue reference
    std::cout << "lvalue reference\n";
}
void f(Clazz&&) {                                                   // accepts rvalue reference
    std::cout << "rvalue reference\n";
}

template <typename T>
void use_lvalue(T&& t) {
    f(t);                                                           // t is treated as l-value unconditionally
}

template <typename T>
void use_rvalue(T&& t) {
    f(std::move(t));                                                // t is treated as r-value unconditionally
}

/* forward restoring original type */
template <typename T>
void use_universal(T&& t) {
    f(std::forward<T>(t));                                          // pass t as r-value if r-value was passed
}                                                                   // pass as l-value otherwise 

int main() {
    const Clazz const_clazz;
    Clazz clazz;
    
    use_universal(const_clazz);                                     // pass const lvalue
    use_universal(clazz);                                           // pass lvalue
    use_universal(Clazz());                                         // pass rvalue    

    return 0;
}
