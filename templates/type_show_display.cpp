#include <iostream>
#include <vector>

class Clazz {};

template<typename T>
auto whatIsTheType() -> void = delete;

int main() {
    std::vector<Clazz> v;

    /* show the type by displaying a compilation error */
    // whatIsTheType<decltype(v)>();

    return 0;
}
