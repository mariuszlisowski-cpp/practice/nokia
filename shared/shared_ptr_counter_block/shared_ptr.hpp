/* shared pointer implementation */
#pragma once

#include <stdexcept>

#include "counter_block.hpp"

template <typename T>
class shared_ptr {
public:
    explicit shared_ptr(T* ptr = nullptr)                           // parametrized c'tor
        : ptr_(ptr), counter_(new counter_block)
    {
        if (ptr) {
            counter_->increment();
        }
    }
    ~shared_ptr() {                                                 // d'tor
        if (!counter_) {
            return;
        }
        cleanup();
    }

    /* copy semantics */ 
    shared_ptr(const shared_ptr& otherPtr) noexcept                 // copy c'tor
        : ptr_(otherPtr.ptr_), counter_(otherPtr.counter_) {
        if (ptr_) {
            counter_->increment();
        }
    }
    shared_ptr& operator=(const shared_ptr& otherPtr) noexcept {    // copy assignment
        if (this != &otherPtr && ptr_ != nullptr) {
            cleanup();
            ptr_ = otherPtr.ptr_;
            counter_ = otherPtr.counter_;
            counter_->increment();
        }

        return *this;
    }

    /* move semantics */
    shared_ptr(shared_ptr&& otherPtr) noexcept                      // move c'tor
        : ptr_(otherPtr.ptr_), counter_(otherPtr.counter_) {
        otherPtr.ptr_ = nullptr;
        otherPtr.counter_ = nullptr;
    }
    shared_ptr& operator=(shared_ptr&& otherPtr) noexcept {         // move assignment
        if (this != &otherPtr && ptr_ != nullptr) {
            cleanup();
            ptr_ = otherPtr.ptr_;
            counter_ = otherPtr.counter_;
            otherPtr.ptr_ = nullptr;
            otherPtr.counter_ = nullptr;
        }

        return *this;
    }

    /* operator overloading */
    T& operator*() const {
        if (!ptr_) {
            throw std::runtime_error("nullptr");
        }

        return *ptr_;
    }

    T* operator->() const noexcept {
        return ptr_;
    }
    operator bool() const noexcept {
        return ptr_ != nullptr;
    }

    /* helper methods */
    T* get() const noexcept {
        return ptr_;
    }
    unsigned use_count() noexcept {
        return counter_->get();
    }
    void reset(T* otherPtr = nullptr) {
        if (!otherPtr) {
            counter_->reset();
        }
        delete ptr_;
        ptr_ = otherPtr;
    }

private:
    T* ptr_;
    counter_block* counter_;

    void cleanup() {
        if (ptr_) {
            counter_->decrement();
        }
        if (use_count() == 0) {
            delete counter_;
            counter_ = nullptr;
            delete ptr_;
        }
    }
};
