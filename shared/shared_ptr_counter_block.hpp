/* shared pointer implementation */
#pragma once

#include <atomic>
#include <stdexcept>

class counter_block {
public:
    counter_block() : counter_(0) {}
    ~counter_block() {}

    counter_block(counter_block&&) = delete;
    counter_block(const counter_block&) = delete;
    counter_block& operator=(const counter_block&) = delete;
    counter_block& operator=(counter_block&&) = delete;

    void increment() noexcept {
        counter_++;
    }
    void decrement() noexcept {
        counter_--;
    }
    unsigned get() const noexcept {
        return counter_.load();
    }
    void reset() {
        counter_ = 0;
    }

private:
    std::atomic<unsigned> counter_;
};

template <typename T>
class shared_ptr {
public:
    explicit shared_ptr(T* ptr = nullptr)                       // parametrized c'tor
        : ptr_(ptr), counter_(new counter_block)
    {
        if (ptr) {
            counter_->increment();
        }
    }
    ~shared_ptr() {                                             // d'tor
        if (!counter_) {
            return;
        }
        cleanup();
    }

    /* copy semantics */ 
    shared_ptr(const shared_ptr& other) noexcept                // copy c'tor
        : ptr_(other.ptr_), counter_(other.counter_) {
        if (ptr_) {
            counter_->increment();
        }
    }
    shared_ptr& operator=(const shared_ptr& other) noexcept {   // copy assignment
        if (this != &other && ptr_ != nullptr) {
            cleanup();
            ptr_ = other.ptr_;
            counter_ = other.counter_;
            counter_->increment();
        }

        return *this;
    }

    /* move semantics */
    shared_ptr(shared_ptr&& other) noexcept                     // move c'tor
        : ptr_(other.ptr_), counter_(other.counter_) {
        other.ptr_ = nullptr;
        other.counter_ = nullptr;
    }
    shared_ptr& operator=(shared_ptr&& other) noexcept {        // move assignment
        if (this != &other && ptr_ != nullptr) {
            cleanup();
            ptr_ = other.ptr_;
            counter_ = other.counter_;
            other.ptr_ = nullptr;
            other.counter_ = nullptr;
        }

        return *this;
    }

    /* operator overloading */
    T& operator*() const {
        if (!ptr_) {
            throw std::runtime_error("nullptr");
        }
        return *ptr_;
    }
    T* operator->() const noexcept {
        return ptr_;
    }
    operator bool() const noexcept {
        return ptr_ != nullptr;
    }

    /* helper methods */
    T* get() const noexcept {
        return ptr_;
    }
    unsigned use_count() noexcept {
        return counter_->get();
    }
    void reset(T* other = nullptr) {
        if (!other) {
            counter_->reset();
        }
        delete ptr_;
        ptr_ = other;
    }

private:
    T* ptr_;
    counter_block* counter_;

    void cleanup() {
        if (ptr_) {
            counter_->decrement();
        }
        if (use_count() == 0) {
            delete counter_;
            counter_ = nullptr;
            delete ptr_;
        }
    }
};
