/* shared pointer implementation */
#pragma once

template <class T>
class shared_ptr {
public:
    shared_ptr()                                            // default constructor
        : ptr_(nullptr), ref_count_(new int(0)) {}
    shared_ptr(T* ptr)                                      // parametrized constructor
        : ptr_(ptr), ref_count_(new int(1)) {}
    ~shared_ptr() {                                         // destructor
        cleanup();
    }

    /* copy semantics */
    shared_ptr(const shared_ptr& other) {                   // copy constructor
        ptr_ = other.ptr_;                                  // share the underlying pointer
        ref_count_ = other.ref_count_;
        if (other.ptr_) {
            (*this->ref_count_)++;                          // if the pointer is not null
        }                                                   // increment the counter
    }
    shared_ptr& operator=(const shared_ptr& other) {        // copy assignment operator
        if (this != &other) {                               // check for self assignment
            cleanup();                                      // cleanup any existing data
            ptr_ = other.ptr_;                              // share the underlying pointer
            ref_count_ = other.ref_count_;
            if (other.ptr_) {
                (*ref_count_)++;                            // if the pointer is not null
            }                                               // increment the counter
        }
        return *this;
    }

    /* move semantics */
    shared_ptr(shared_ptr&& other) {                        // move constructor
        ptr_ = other.ptr_;                                  // share the underlying pointer
        ref_count_ = other.ref_count_;
        other.ptr_ = nullptr;                               // clean the dying object
        other.ref_count_ = nullptr;
    }
    shared_ptr& operator=(shared_ptr&& other) {             // move assignment operator
        if (this != &other) {                               // check for self assignment
            cleanup();                                      // cleanup any existing data
            ptr_ = other.ptr_;                              // share the underlying pointer
            ref_count_ = other.ref_count_;
            other.ptr_ = nullptr;                           // clean the dying object
            other.ref_count_ = nullptr;
        }

        return *this;
    }

    /* operator overloading */
    T* operator->() const { return this->ptr_; }
    T& operator*() const { return this->ptr_; }

    /* helper methods */
    int get_count() const { return *ref_count_; }
    T* get() const { return ptr_; }

private:
    T* ptr_ = nullptr;
    int* ref_count_ = nullptr;

    void cleanup() {
        (*ref_count_)--;
        if (*ref_count_ == 0) {
            if (ptr_) {
                delete ptr_;
            }
            delete ref_count_;
        }
    }
};
