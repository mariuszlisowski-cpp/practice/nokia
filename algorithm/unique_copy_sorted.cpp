#include <algorithm>
#include <cctype>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main() {
    std::string str{ "a  b  c   d e    " };

    std::sort(str.begin(), str.end());
    
    /* copy without whitespaces */
    std::unique_copy(str.begin(), str.end(),
                     std::ostream_iterator<char>(std::cout),
                     [](const auto& prev, const auto& curr) {
                         return std::isspace(prev) && std::isspace(curr);   // no whitespaces
                     });

    std::cout << "\n\'" << str << '\'' << std::endl;                        // original string intact
                                                                            // but sorted with spaces
    /* remove whitespacesfrom original */
    auto it = std::unique(str.begin(), str.end(),
                          [](const auto& prev, const auto& curr) {
                              return std::isspace(prev) && 
                                     std::isspace(curr);
                          });

    /* erase/remove idiom needed */
    std::cout << '\n' << str << '\'' << std::endl;                          // original with rubbish
    str.erase(it, str.end());                                               // erase rubbish at the end of string
    std::cout << str << '\'' << std::endl;                                  // original string changed

    return 0;
}
