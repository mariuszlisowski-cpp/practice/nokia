
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v1{ 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 };
    std::vector<int> v2{ 11, 22, 33};

    int x{ 11 };                                                        // initialize lvalue
    std::swap(x, v1[0]);                                                // swap values (lvalues)
    std::cout << "first: " << v1[0] << std::endl;

    std::iter_swap(v1.begin(), v1.end() - 1);                           // swap iterators (first with last)
    std::cout << "first now: " << v1.front() << std::endl;
    std::cout << "last now:  " << v1.back() << std::endl;

    std::swap_ranges(v1.begin() + 1, v1.begin() + 4,                    // swap indices [1, 3]
                     v2.begin());
    std::copy(v2.begin(), v2.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
