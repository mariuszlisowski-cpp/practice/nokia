#include <algorithm>
#include <functional>
#include <ios>
#include <iostream>
#include <vector>

template<int x>
auto greater_than = std::bind(std::greater<>(), std::placeholders::_1, x);


void print(bool result) {
    std::cout << std::boolalpha << result << std::endl;
}

int main() {
    std::vector<int> v{ 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 };

    print(std::all_of(v.begin(), v.end(), greater_than<5>));                // are all greater than... NO
    print(std::none_of(v.begin(), v.end(), greater_than<5>));               // are all smaller than... NO
    print(std::any_of(v.begin(), v.end(), greater_than<5>));                // is any of greater than... YES



    return 0;
}
