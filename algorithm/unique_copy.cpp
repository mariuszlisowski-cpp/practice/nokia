#include <algorithm>
#include <cctype>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main() {
    std::string str{ "1      2   3   " };

    /* remove multiple spaces & whitespaces (also LFs) */
    std::unique_copy(str.begin(), str.end(),
                     std::ostream_iterator<char>(std::cout),
                     [](const auto& prev, const auto& curr) {
                         return std::isspace(prev) && std::isspace(curr);
                     });

    std::cout << '\n' << str << std::endl;                                  // original string intact

    /* erase/remove idiom needed */
    auto it = std::unique(str.begin(), str.end(),
                          [](const auto& prev, const auto& curr) {
                              return std::isspace(prev) && 
                                     std::isspace(curr);
                          });
    str.erase(it, str.end());                                               // erase rubbish at the end
    std::cout << str << std::endl;                                          // original string changed

    return 0;
}
