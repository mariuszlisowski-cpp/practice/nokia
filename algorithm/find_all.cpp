#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

template<int n>
auto is_greater_than = std::bind(std::greater<>(), std::placeholders::_1, n);

int main() {
    std::vector<int> v{ -9, - 7, 3 , 4 , 2 , 6 , 7 };

    std::vector<int>::iterator it{ v.begin() };
    while (it != v.end()) {
        it = std::find_if(it, v.end(), is_greater_than<1>);
        std::cout << *it << ' ';
        ++it;
    }

    return 0;
}
