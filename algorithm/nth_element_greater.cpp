/* nth_element is a partial sorting algorithm that rearranges elements in container such that:
   a. the element pointed at by nth is changed to whatever element would occur in that position if were sorted
   b. all of the elements before this new nth element are less than or equal to the elements after the new nth element
 */

#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 1, 7, 1, 6, 5, 4, 3, 9, 9 };
    /*          index:  0  1  2 |it|                                        // would be here
                      { 9, 9, 7 }                                           // std::greater comparator
                        greater |    less or equal                              
                       (sorted)       (not sorted)
     */

    auto it = std::next(v.begin() + 2);                                     // three elements
    std::cout << "past the range: " <<  *it << std::endl;                                      
    std::nth_element(v.begin(),                                             // first
                     it,                                                    // sort up to it (excluded)
                     v.end(),
                     std::greater<>());                                     // custom comparator

    std::copy(v.begin(), it,                                                // print sorted (guaranteed)
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
