/* Reorders the elements in the range in such a way that all elements for which 
   the predicate returns TRUE precede the elements for which predicate returns FALSE. 
   Relative order of the elements is NOT preserved. */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

using Iter = std::vector<int>::const_iterator;

void print(Iter it1, Iter it2) {
    std::copy(it1, it2,
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    /* relative order NOT preserved */
    std::vector<int> v{ 1, 5, 6, 1, 3, 6, 1, 8, 4, 9, 5, 3 };
    //                  9 8 6 6                                             // example output
    //                  3 1 1 5 4 1 5 3                                     // saa
    std::cout << "size: " <<  v.size() << std::endl;

    auto partition_point =                                                  // iterator to range mid point
        std::partition(v.begin(), v.end(),
                       [](const auto& el) {
                           return el > 5;                                   // to the left if TRUE
                       });
    
    print(v.begin(), partition_point);                                      // left half (bigger than)
    print(partition_point, v.end());                                        // right half (smaller or equal)

    return 0;
}
