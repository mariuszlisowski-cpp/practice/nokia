#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    /* regular sort */
    std::vector<int> v{ 1, 2, 5, 1, 6, 2, 5, 7, 9, 2, 5 };
    std::cout << std::boolalpha << std::is_sorted(v.begin(), v.end()) << '\n';
    
    std::sort(v.begin(), v.end());
    std::cout << std::boolalpha << std::is_sorted(v.begin(), v.end()) << '\n';

    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    /* stable sort */
    std::vector<std::pair<int, int>> pairs{                                         // will be sorted by the key
        {2, 8},
        {1, 9},
        {2, 6},
        {3, 5},
        {2, 7},
        // {4, 4}
    };

    // std::sort(pairs.begin(), pairs.end());
    std::stable_sort(pairs.begin(), pairs.end());                                   // the order of equivalent elements
                                                                                    // is guaranteed to be preserved
    for (auto [first, second] : pairs) {
        std::cout << first << " : " << second << std::endl;
    }

    return 0;
}
