#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 97, 98, 99 };
    std::copy_n(v.begin(), v.size(),                                // n as size of a vector (whole)
                std::ostreambuf_iterator<char>(std::cout));

    std::copy_n(v.begin(), v.size(),                                // n as size of a vector (whole)
                std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
