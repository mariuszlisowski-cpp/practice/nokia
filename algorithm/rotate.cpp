#include <algorithm>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

void print(size_t offset, const std::vector<int>& v) {
    std::cout << std::string(offset, ' ');
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> v{ 1 , 2 , 3 , 4 , 5 , 6 , 7, 8 , 9 , 10 };
    print(0, v);

    /* rotates left */
    std::rotate(v.begin(),
                v.begin() + 3,                                              // becomes the first element
                v.end());

    print(6, v);
    
    return 0;
}
