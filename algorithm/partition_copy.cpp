/* Reorders the elements in the range in such a way that all elements for which 
   the predicate returns TRUE precede the elements for which predicate returns FALSE. 
   Both destinations are filled by conserving the original ordering. */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

using Iter = std::vector<int>::const_iterator;

void print(Iter it1, Iter it2) {
    std::copy(it1, it2,
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    /* relative order is preserved */
    std::vector<int> v{ 1, 5, 6, 1, 3, 6, 1, 8, 4, 9, 5, 3 };
    //                  1  5     1  3     1        9  5  3                  // odds
    //                        6        6     8  4                           // evens

    std::vector<int> evens;
    std::vector<int> odds;

    std::partition_copy(v.begin(), v.end(),
                        std::back_inserter(evens),
                        std::back_inserter(odds),
                        [](const auto& el) {
                            return el % 2;                                  // separates odds & evens
                        });
    
    print(evens.begin(), evens.end());
    print(odds.begin(), odds.end());

    return 0;
}
