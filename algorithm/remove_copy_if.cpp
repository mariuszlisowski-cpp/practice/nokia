#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 6 };

    auto it = std::remove_if(v.begin(), v.end(), 
                             [](const auto& el) {
                                 return !(el % 2);                          // all odd elements
                             });                                            // iterator to past remaining elements

    v.erase(it, v.end());                                                   // erase unwanted elements

    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));

    auto past_copied = 
        std::remove_copy_if(v.begin(), v.end(), 
                           std::ostream_iterator<int>(std::cout, " "),      // print...
                           [](const auto& el) {
                               return el % 2;                               // all even elements
                           });

    // v.erase(past_copied, v.end());                                       // no need to erase

    std::cout << "\n" << v.size() << std::endl;

    return 0;
}
