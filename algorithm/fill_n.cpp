#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> v(10);                                        // n elements

    /* fill half a vector */
    auto it = std::fill_n(v.begin(), v.size() / 2, "filled");              // retruns past the range iterator

    std::fill(it, v.end(), ".");                                           // fill the rest

    std::copy(v.begin(), v.end(),
              std::ostream_iterator<std::string>(std::cout, " | "));

    return 0;
}
