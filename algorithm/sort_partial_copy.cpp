/* Sorts some of the elements in the range [first, last) in ascending order,
   storing the result in the range [d_first, d_last). */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    /*          index:  0  1  2  3  4  5  6  7  8                           // nine elements (intact)
                                   mid
                  out:  6  7  8  9
    */
    std::cout << "half size: " << v.size() / 2 << std::endl;
    auto middle = std::next(v.begin(), v.size() / 2);                       // five elements (0-4)
    std::cout << "half value: " << *middle << std::endl;

    /* copying partially sorted */
    std::vector<int> v_sorted(v.size() / 2);                                // size is four
    
    std::partial_sort_copy(v.begin(), middle,                               // middle excluded
                           v_sorted.begin(), v_sorted.end());


    std::copy(v_sorted.begin(), v_sorted.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
