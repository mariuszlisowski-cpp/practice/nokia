/* searches for one container contained in another */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    //                                    _________       __________
    std::vector<int> long_vector{ 7 , 3 , 1 , 2 , 3 , 6 , 1 , 2 , 3, 10 };
    std::vector<int> short_vector{ 1, 2, 3 };

    /* first occurence of one vector in another */
    auto it = std::search(long_vector.begin(), long_vector.end(),               // is short vector contained
                          short_vector.begin(), short_vector.end());            // in a long one?
    
    if (it != long_vector.end()) {
        std::cout << "first occurence of 'short' in 'long' at index " 
                  << std::distance(long_vector.begin(), it) << std::endl;
    }

    /* last occurence of one vector in another */
    it = std::find_end(long_vector.begin(), long_vector.end(),                  // is short vector contained
                       short_vector.begin(), short_vector.end());               // in a long one?
    
    if (it != long_vector.end()) {
        std::cout << "second occurence of 'short' in 'long' at index " 
                  << std::distance(long_vector.begin(), it) << std::endl;
    }

    return 0;
}
