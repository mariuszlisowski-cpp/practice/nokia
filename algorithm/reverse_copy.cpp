#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main() {
    std::string str{ "1 2 3" };

    std::reverse(str.begin(), str.end());                               // nothing returned
    std::cout << str << std::endl;

    auto past_copied =                                                  // iterator to the element
        std::reverse_copy(str.begin(), str.end(),                       // past the last element copied
                          std::ostream_iterator<char>(std::cout));      // string remains intact


    return 0;
}
