#include <algorithm>
#include <iostream>
#include <type_traits>
#include <vector>

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 9, 8, 7, 6 };
    /*                  1  2  3  4  5  9  ^ 
                                        | sorted until
                                        | unsorted
    */
    auto until = std::is_sorted_until(v.begin(), v.end());
    std::cout << "sorted till value: " << *until
              << ", at index: " << std::distance(v.begin(), until);

    return 0;
}
