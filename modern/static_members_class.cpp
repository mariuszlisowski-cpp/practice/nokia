/* When a MEMBER of a class is declared as STATIC it means no matter how many objects of the class are created
   there is only ONE COPY of the static member.

   By declaring a FUNCTION MEMBER as STATIC, you make it independent of any particular object of the class.
   A static member function can be called even if no objects of the class exist
   and the static functions are accessed using only the class name and the scope resolution operator (::).

   Static member functions have a class scope and they do not have access to the 'this' pointer of the class.
 */
#include <iostream>

class Box {
public:
    static int object_count;                                                // static variable member
                                                                            // shared by all objects of the class
    Box(double l = 2.0, double b = 2.0, double h = 2.0) {
        length = l;
        breadth = b;
        height = h;

        object_count++;                                                     // increase every time object is created
    }
    double volume() { return length * breadth * height; }
    static int get_count() { return object_count; }                         // static method member

private:
    double length;
    double breadth;
    double height;
};

int Box::object_count = 0;                                                  // initialize static member of class Box

int main(void) {
    std::cout << "Inital Stage Count: "
              << Box::get_count() << std::endl;                             // accessing static method
                                                                            // before creating object

    Box box1(3.3, 1.2, 1.5);                                                // first object
    Box box2(8.5, 6.0, 2.0);                                                // second object (shares obejct_count)

    std::cout << "> total objects created: "
              << Box::object_count << std::endl;                            // accessing static member

    std::cout << "Final Stage Count: "
              << Box::get_count() << std::endl;                             // accessing static method
                                                                            // after creating object
    
    std::cout << box1.volume() << std::endl;                                // regular method call
    std::cout << box1.get_count() << std::endl;                             // static method can be also accessed
    std::cout << box2.get_count() << std::endl;                             // as regular method
    
    return 0;
}
