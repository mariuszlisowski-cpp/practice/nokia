/* 1. In C++11, in addition to copy constructors, objects can have move constructors.
      And in addition to copy assignment operators, they have move assignment operators.
   2. The move constructor is used instead of the copy constructor,
      if the object has type "rvalue-reference" (Type &&).
   3. std::move() is a cast that produces an rvalue-reference to an object,
      to enable moving from it. 
      
   std::move() doesn't actually move anything!
   It changes an expression from being an lvalue (such as a named variable) to being an xvalue
   which is eXpiring value (general lvalue that denotes an object whose resources can be reused)

   std::move returns T&& no matter if called with a value (T), reference type (T&), or rvalue reference (T&&).
   
   You should use std::move if you want to call functions that support move semantics with an argument
   which is not an rvalue (temporary expression).
*/
#include <cstddef>
#include <iostream>
constexpr size_t SIZE{ 10 };

class Clazz {
public:
    Clazz() {
        std::cout << "> default c'tor\n";
        buffer_ = new int[SIZE];
    }
    ~Clazz() {
        delete [] buffer_;
    }
    Clazz(const Clazz& other) : Clazz() {                                   // allocate new memory by default c'tor
        std::cout << "> copy c'tor\n";
        memcpy(buffer_, other.buffer_, sizeof(*buffer_) * SIZE);            // copying buffer
    }
    Clazz(Clazz&& other) {                                                  // move c'tor
        std::cout << "> move c'tor\n";
        // delete [] buffer_;                                               // NO buffer not initialized yet!
        buffer_ = other.buffer_;                                            // changing ownership only
        other.buffer_ = nullptr;
    }
    Clazz& operator=(const Clazz& other) {                                  // copy assignment
        if (this != &other) {                                               // self-assignment check
            std::cout << "> copy assignment\n";
            Clazz temp(other);                                              // use copy c'tor
            std::swap(temp, *this);                                         // a copy becomes a current object
        }
        return *this;
    }
    Clazz& operator=(Clazz&& other) {                                       // move assignment
        if (this != &other) {
            std::cout << "> move assignment\n";
            delete [] buffer_;                                              // deallocate current memory
            buffer_ = other.buffer_;                                        // switch to new memory address
            other.buffer_ = nullptr;                                        // do not use the other pointer
        }

        return *this;
    }
// private:
    int* buffer_;
};

/* slightly simplified std::move()
   remove_reference converts T& to T or T&& to T */
template<typename T>
typename std::remove_reference<T>::type&&
move(T&& t) noexcept {
    typedef typename std::remove_reference<T>::type _Up;
    return static_cast<_Up&&>(t);
}

int main() {
    Clazz a;                                                                // a is lvalue
    
    Clazz b = move(a);                                                      // using move c'tor
    if (a.buffer_ == nullptr) {
        std::cout << "# nullptr" << std::endl;                              // DO NOT use object 'a'
    }
    Clazz c = static_cast<Clazz&&>(b);                                      // direct casting (instead of move)
                                                                            // DO NOT use object 'b'
    Clazz d;
    c = std::move(d);                                                       // using move assignment operator
                                                                            // DO NOT use object 'c'
    Clazz e;
    std::cout << "~~~~~~~~~~~~~~~~~" << std::endl;
    c = e;                                                                  // calls copy assignment operator
                                                                            // which calls copy c'tor with default one
                                                                            // then std::swap is called
                                                                            // which calls move c'tor
                                                                            // and move assignment operator (twice)
    return 0;
}
