/* A simple rule to follow in data member order declaration to minimize additional padding
   is to declare data members in descending order of size (as returned by the sizeof operator)
*/
#include <iostream>

struct A {
    char c1;
    short s1;
    int i1;
    char c2;
};

struct B {                              // data alignment done by compiler 
    char c1;
    // 1-byte padding
    short s1;
    int i1;
    char c2;
    // 3-byte padding
};

/* minimization: descending order */
struct C {
    int i1;                             // longest size
    short s1;
    short c1;
    char c2;
    // 3-byte padding
};

int main() {
    /* operators
    alignof);
    alignas; */
    
    /* functions
    std::alignment_of();
    std::aligment_of_v();
    std::align();
    std::aligned_storage();
    std::aligned_union(); */
    
    /* types
    std::align_val_t
    std::max_align_t */

    return 0;
}
