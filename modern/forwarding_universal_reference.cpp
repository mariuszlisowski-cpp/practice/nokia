#include <string>

class Person {
public:
    Person(std::string name, std::string surname)							// parametrized c'tor
        : m_name{std::move(name)}
        , m_surname{std::move(surname)}
    {}

private:
    std::string m_name, m_surname;
};

class StringBuilder {
public:
    StringBuilder(std::string& destination)
        : m_destination{std::move(destination)}
    {}

private:
    std::string m_destination;
};

/* passing by copy */
template <typename T, typename... Args>
T construct_v1(Args... args) {
	return T{args...};														// returns Person (using c'tor)
}

/* passing by const reference */
template <typename T, typename... Args>
T construct_v2(const Args&... args) {
	return T{args...};                                                      // but the 1st argument...
}                                                                           // is copied while object creation

/* forwarding/universal reference */
template <typename T, typename... Args>
T construct_v3(Args&&... args) {                                            // 1st parameter is moved (string&&)
	return T{std::move(args)...};											// 2nd deduced: const std::string&
}																			// for the second parameter
                                                                            // which is copied (as it's const)

int main() {
    auto name1 = std::string{"Jan"};
    auto name2 = std::string{"Jan"};
    auto name3 = std::string{"Jan"};
    const auto surname = std::string{"Kowalski"};

    auto p1 = construct_v1<Person>(std::move(name1), surname);
    auto p2 = construct_v2<Person>(std::move(name2), surname);				// 2nd argument is a 'const string'
    
    /* forwarding reference works */
	auto p3 = construct_v3<Person>(std::move(name3), surname);				// forwarding reference or...
 																			// universal reference is use
    /* not for use cases; use perfect forwarding for all cases ! */
    auto str = std::string{};
    // auto builder = construct_v3<StringBuilder>(str);                     // ERROR: cannot bind non-const lvalue
                                                                            // reference of type ‘std::string&’ to an
                                                                            // rvalue of [...] {aka ‘std::string’} !
                                                                            // Need for perfect forwarding
    return 0;
}
