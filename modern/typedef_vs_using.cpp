#include <iostream>
#include <map>
#include <unordered_map>

class CheckingAccount {};
class SavingsAccount {};

/* typef before C++11 */
typedef int UserId;																	// user identifier type alias
typedef std::map<UserId, std::vector<CheckingAccount>> UserCheckingAccounts_t;		// define collection type aliases
typedef std::map<UserId, std::vector<SavingsAccount>> UserSavingsAccounts_t;

UserCheckingAccounts_t UserCheckingAccounts;										// define maps
UserSavingsAccounts_t UserSavingsAccounts;

void process(UserSavingsAccounts_t& userAccountsMap);								// type alias as function parameter

/* alias declaration from C++11 */
template<typename A>																// a template or generic alias
using UserAccounts_t = std::map<UserId, std::vector<A>>;

using UserCheckingAccounts_t = UserAccounts_t<CheckingAccount>;
using UserSavingsAccounts_t = UserAccounts_t<SavingsAccount>;

template<typename A>																// a template or generic alias
using UserAccounts_t = std::map<UserId, std::vector<A>>;
/* easy change std::map to std::unordered_map
template<typename A>
using UserAccounts_t = std::unordered_map<UserId, std::vector<A>>; */

using UserCheckingAccounts_t = UserAccounts_t<CheckingAccount>;
using UserSavingsAccounts_t = UserAccounts_t<SavingsAccount>;



template<typename A>																// ugly generic alias with typedef
struct UserAccounts_type {
 typedef std::map<UserId, std::vector<A>> type;
};

int main() {
	return 0;
}
