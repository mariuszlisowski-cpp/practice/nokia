/* The cornerstone of move semantic is the ability to detect during a "copy" if the source object will be reused or not.
   There are three situations:
   - The object is a temporary object, with no name, and if it can’t be named, it can’t be used
   - The object is used in some specific places, such as a return statement
   - The user explicitly promises to the compiler that he won’t care for the current value of the object any longer.
     He does so by using the specific cast operation named std::move.
*/
#include <vector>

class A {};

A getA() {
    return A();
};

A non_compliant() {
  A a = std::move(getA());                                          // prevents copy elision
  std::vector<A> v;
  v.push_back(std::move(getA()));                                   // no need to move

  return std::move(a);                                              // prevents copy elision
}


A compliant() {
  A a = getA();                                                     // OK
  std::vector<A> v;
  v.push_back(getA());                                              // OK
  
  return a;                                                         // OK
}
