#include <iostream>
#include <map>

using namespace std;

int main(void) {
    /* Initializer_list constructor */
    map<char, int> m = {
        {'a', 1},
        {'b', 2},
        {'c', 3},
        {'d', 4},
        {'e', 5},
    };

    cout << "Map contains following elements" << endl;

    for (auto it = m.begin(); it != m.end(); ++it)
        cout << it->first << " = " << it->second << endl;

    std::cout << (--m.end())->first << std::endl;

    return 0;
}
