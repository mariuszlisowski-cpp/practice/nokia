#include <iostream>

/*** universal reference ***/
template<typename T>
void f(T&& param);               // deduced parameter type ⇒ type deduction;
                                 // && ≡ universal reference
 
template<typename T1>
class Gadget {
    template<typename T2>
    Gadget(T2&& rhs);            // deduced parameter type ⇒ type deduction;
                                 // && ≡ universal reference
};

/*** rvalue reference ***/
template<typename T>
class Widget {
    Widget(Widget&& rhs);        // fully specified parameter type ⇒ no type deduction;
                                 // && ≡ rvalue reference
};
 
class Clazz {};

void f(Clazz&& param);          // fully specified parameter type ⇒ no type deduction;
                                // && ≡ rvalue reference

template <class T, 
          class Allocator = std::allocator<T>>
class vector {
public:
    void push_back(T&& x);       // fully specified parameter type ⇒ no type deduction;
                                 // && ≡ rvalue reference
};
