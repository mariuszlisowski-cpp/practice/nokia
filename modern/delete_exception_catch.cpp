#include <exception>
#include <iostream>

class CPerson {
  public:
    void someFunc() {
        throw std::exception();
    }
};

void func() {
    CPerson* myPerson = new CPerson;

    try {
        // do something that might throw an exception
        myPerson->someFunc();
    } catch (const std::exception& e) {
        e.what();
        // destroy the object before passing exception on
        delete myPerson;
        std::cout << "destroyed in catch" << std::endl;
        // throw the exception to the next handler
        throw;
    }

    // on normal exits, destroy the object
    delete myPerson;
    std::cout << "destroyed normally on exit" << std::endl;
}

int main() { 
    try {
        func();
    } catch (...) {
        std::cout << "caught in main" << std::endl;
    }

    return 0;
}
