/* std::move converts
   - value (T)
   - reference type (T&)
   to rvalue reference (T&&)
   and leaves intact 
   - rvalue reference (T&&)
   
   You should use std::move if you want to call functions that support move semantics with an argument
   which is not an rvalue (temporary expression).
*/
#include <iostream>

template<typename T>
void foo(T& obj) {                                                          // accepts lvalues only
    std::cout << "> lvalue reference" << std::endl;
}

template<typename T>
void foo(T&& obj) {                                                         // supports move semantics
    std::cout << "> rvalue reference" << std::endl;
}

int main() {
    int a{ 42 };                                                            // lvalue
    int& b = a;

    foo(a);                                                                 // lvalue passed (T)
    foo(b);                                                                 // reference passed (T&)

    /* utilizing move semantics from overloaded foo */
    foo(std::move(a));                                                      // lvalue converted to rvalue
    foo(std::move(b));                                                      // reference passed (T&) and converted
    foo(42);                                                                // rvalue passed (no conversion needed)

    return 0;
}
