/* As of C++17, Copy Elision is guaranteed when an object is returned directly */

#include <iostream>

struct C {
    C() {}
    C(const C&) { std::cout << "A copy was made.\n"; }          // not called althought procudes side effect
};

C performing_copy_elision() {
    return C();                                                 // definitely performs copy elision
}                                                               // as object is rerurned directly

C maybe_performing() {
    C c;
    return c;                                                   // maybe performs copy elision
}

int main() {
    C obj = performing_copy_elision();                          // copy constructor isn't called
}
