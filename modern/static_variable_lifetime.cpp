/* The space for the static variable is allocated only one time and this is used for the entirety of the program.
   Once this variable is declared, it exists till the program executes.
   So, the lifetime of a static variable is the lifetime of the program.
*/
#include <iostream>

void func_with_static() {
    static int number{};                                                    // initialized once
    std::cout << "number: " << number << "\n";
    number++;                                                               // value survives
}

void func_without_static() {
    int value{};                                                            // unitialized every function call
    std::cout << "value: " << value << "\n";                                // thus same value every call
    value++;                                                                // value is lost
}

int main() {
    func_with_static();
    func_with_static();
    func_with_static();

    func_without_static();
    func_without_static();
    func_without_static();

    return 0;
}
