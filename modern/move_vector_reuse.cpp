#include <vector>

void consume(std::vector<int> vec) {}

int main() {
    std::vector<int> vec{1, 2, 3, 4};

    consume(std::move(vec));

    // here the vec object is in an indeterminate state
    // since the object is not destroyed, we can assign it a new content
    // we will, in this case, assign an empty value to the vector, making it effectively empty
    vec = {};

    // since the vector as gained a determinate value, we can use it as usual
    vec.push_back(42);
}
